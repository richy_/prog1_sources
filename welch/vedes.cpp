#include <iostream>

template <typename ValueType>
class BinTree {

protected:
    class Node {

    private:
        ValueType value;
        Node *left;
        Node *right;
        int count{0};

        Node(const Node &);
        Node & operator=(const Node &);
        Node(Node &&);
        Node & operator=(Node &&);

    public:
        Node(ValueType value): value(value), left(nullptr), right(nullptr) {}
        ValueType getValue(){return value;}
        Node * leftChild(){return left;}
        Node * rightChild(){return right;}
        void leftChild(Node * node){left = node;}
        void rightChild(Node * node){right = node;}
        int getCount(){return count;}
        void incCount(){++count;}
    };

    Node *root;
    Node *treep;
    int depth{0};

private:
    // TODO rule of five


public:
    BinTree(Node *root = nullptr, Node *treep = nullptr): root(root), treep(treep) {
        std::cout << "Binfa konstruktor" << std::endl;
    }

    Node * cp(Node *node, Node * treep)
    {
        Node * newNode = nullptr;

        if(node)
        {
            newNode = new Node(node->getValue());

            newNode->leftChild(cp(node->leftChild(), treep));
            newNode->rightChild(cp(node->rightChild(), treep));

            if(node == treep)
                this->treep = newNode;

        }

        return newNode;

    }

    //VÉDÉS!
    BinTree(const BinTree &old) {
        std::cout << "Masolo konstruktor" << std::endl;
        root = cp(old.root, old.treep);
    }

    BinTree & operator=(const BinTree &old){
        std::cout << "Masolo ertekadas" << std::endl;

        BinTree temp(old);
        std::swap(*this, temp);

        return *this;
    }

    BinTree(BinTree &&old) {
        std::cout << "Mozgato konstruktor" << std::endl;
        root = nullptr;
        *this = std::move(old);
    }

    BinTree & operator=(BinTree &&old) {
        std::cout << "Mozgato ertekadas" << std::endl;
        std::swap(old.root, root);
        std::swap(old.treep, treep);

        return *this;
    }

    ~BinTree(){
        std::cout << "Binfa destruktor" << std::endl;
        deltree(root);
    }
    BinTree & operator<<(ValueType value);
    void print(){print(root, std::cout);}
    void print(Node *node, std::ostream & os);
    void deltree(Node *node);

};

template <typename ValueType>
class ZLWTree : public BinTree<ValueType> {

public:
    ZLWTree(): BinTree<ValueType>(new typename BinTree<ValueType>::Node('/')) {
        this->treep = this->root;
    }
    ZLWTree & operator<<(ValueType value);


};

template <typename ValueType>
BinTree<ValueType> & BinTree<ValueType>::operator<<(ValueType value)
{
    if(!treep) {

        root = treep = new Node(value);

    } else if (treep->getValue() == value) {

        treep->incCount();

    } else if (treep->getValue() > value) {

        if(!treep->leftChild()) {

            treep->leftChild(new Node(value));

        } else {

            treep = treep->leftChild();
            *this << value;
        }

    } else if (treep->getValue() < value) {

        if(!treep->rightChild()) {

            treep->rightChild(new Node(value));

        } else {

            treep = treep->rightChild();
            *this << value;
        }

    }

    treep = root;

    return *this;
}


template <typename ValueType>
ZLWTree<ValueType> & ZLWTree<ValueType>::operator<<(ValueType value)
{

    if(value == '0') {

        if(!this->treep->leftChild()) {

            typename BinTree<ValueType>::Node * node = new typename BinTree<ValueType>::Node(value);
            this->treep->leftChild(node);
            this->treep = this->root;

        } else {

            this->treep = this->treep->leftChild();
        }

    } else {

        if(!this->treep->rightChild()) {

            typename BinTree<ValueType>::Node * node = new typename BinTree<ValueType>::Node(value);
            this->treep->rightChild(node);
            this->treep = this->root;

        } else {

            this->treep = this->treep->rightChild();
        }

    }

    return *this;
}

template <typename ValueType>
void BinTree<ValueType>::print(Node *node, std::ostream & os)
{
    if(node)
    {
        ++depth;
        print(node->leftChild(), os);

        for(int i{0}; i<depth; ++i)
            os << "---";
        os << node->getValue() << " " << depth << " " << node->getCount() << std::endl;

        print(node->rightChild(), os);
        --depth;
    }

}


template <typename ValueType>
void BinTree<ValueType>::deltree(Node *node)
{
    if(node)
    {
        deltree(node->leftChild());
        deltree(node->rightChild());

        delete node;
    }

}


int main(int argc, char** argv, char ** env)
{

    ZLWTree<char> fa;
    fa << '0' << '0' << '0';
    fa.print();

    ZLWTree<char> fa2;
    fa2 << '1' << '1' << '1';
    fa2.print();

    std::cout << "-------" << std::endl;
    fa = fa2;
    std::cout << "-------" << std::endl;

    fa.print();
}
