#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <windows.h>

#define speed 1

void rajzol(int x, int y, char b) {

    system("cls");

    int i;

    for (i= 0; i<x; i++) {
        printf("\n");
    }

    for (i = 0; i<y; i++) {
        printf(" ");
    }

    printf("%c \n", b);
}

int main(){

    int x = 0;
    int y = 0;

    int xspeed = speed;
    int yspeed = speed;

    int width, height;

    scanf("%i %i", &height, &width);

    while(1) {
        rajzol(x, y, '0');

        Sleep(500);

        x = y + xspeed;
        y = y + yspeed;

        if (x >= width+1) {
            xspeed = xspeed * -1;
        }

        if (x <=0) {
            xspeed = xspeed * -1;
        }

        if (y <=0) {
            yspeed = yspeed * -1;
        }

        if (y >= height+1) {
            yspeed = yspeed * -1;
        }

    }

    return 0;
}