#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <windows.h>

int labda(int x, int y) {
    int i;

    for(i=0; i<x; i++) {
        printf("\n");
    }

    for(i=0; i<y; i++) {
        printf(" ");
    }

    printf("O\n");

    return 0;
}

int main() {

    int x = 0, y = 0;
    int width, height;

    printf("Add meg a magassagot es a szelesseget...");
    scanf("%i %i", &height, &width);

    while(1) {
        system("cls");
        labda(abs(height-(x++%(height*2))), abs(width-(y++%(width*2))));
        Sleep(500);
    }

    return 0;
}