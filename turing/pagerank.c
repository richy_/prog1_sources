#include "stdlib.h"
#include <stdio.h>
#include <math.h>

void kiir(double tomb[]) {
    //Irasd ki a parameterkent kapott tomb elemeit

    for (int i = 0; i< 4; i++) {
        printf("%f",tomb[i]);
        printf("\n");
    }
}

double tavolsag(double PR[], double PRv[]) {

    //Itt volt a kod

    double osszeg = 0.0;

    for (int i = 0; i < 4; i++) {
        osszeg += fabs(PR[i] - PRv[i]);
    }

    return osszeg;
}

int main(void) {
    double L[4][4] = {
		{0.0, 0.0, 1.0 / 3.0, 0.0},
		{1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0},
		{0.0, 1.0 / 2.0, 0.0, 0.0},
		{0.0, 0.0, 1.0 / 3.0, 0.0}
	};


    double PR[4] = {0.0, 0.0, 0.0, 0.0};
	double PRv[4] = {1.0/4.0,1.0/4.0,1.0/4.0,1.0/4.0};


    int i, j;

    for(;;) {
    //Itt volt a kod

        for (i = 0; i < 4; i++) {
            PR[i] = PRv[i];
        }

        for (i = 0; i<4; i++) {
            double temp = 0;

            for (j = 0; j<4; j++) {
                temp+=L[i][j]*PR[j];
            }

            PRv[i] = temp;
        }

        if (tavolsag(PR, PRv) < 0.00001)
            break;

    }

    kiir (PR);

    return 0;
}